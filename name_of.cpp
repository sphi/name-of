#include <string>
#include <vector>
#include <iostream>
#include <ctime>

// Given a pointer, returns a human-readable name. Useful for debugging, since
// names are easier to remember and recognise than memory addresses. 0 results
// in "Null", and Names of inputs that are close together often rhyme.
std::string name_of(const void* input) {
    using std::string;

    static const std::vector<std::string> beginnings {
        "N", "Al", "Aver", "B", "Ch", "D", "Ell", "Ev", "F", "G", "Gr", "H",
        "Iv", "J", "K", "L", "M", "Mr ", "Ms ", "Mx ", "P", "Pr", "Qu", "R",
        "S", "T", "Tr", "V", "W", "Z"
    };

    static const std::vector<std::string> middles {
        "u", "a", "ai", "ay", "e","ee", "el", "en", "i", "ing", "o", "oo",
        "ou", "or"
    };

    static const std::vector<std::string> ends {
        "ll", "b", "ck", "cy", "d", "dy", "gh", "gia", "r", "l", "le", "liet",
        "lie", "mer", "nd", "ny", "rd", "sly", "son", "ss", "thy", "te", "tt",
        "xon", "y", "yton", "zz", ""
    };

    unsigned long int id = (unsigned long int)input;
    string result;

    result += beginnings[id % beginnings.size()];
    id /= beginnings.size();

    result += middles[id % middles.size()];
    id /= middles.size();

    result += ends[id % ends.size()];
    id /= ends.size();

    return result;
}

int main() {
    srand(time(NULL));
    for (int i = 0; i < 20; i++) {
        unsigned long long num = ((unsigned long long)rand() << 32) | rand();
        std::cout << name_of((void*)num) << std::endl;
    }
}

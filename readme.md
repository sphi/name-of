A small utility function for converting memory addresses (or any numerical IDs) into human-readable names.

For example, the following log:
```
Setting focus to 0x7ffcbd27df80 (parent: 0x55e467b563c0)
0x7ffcbd27df80 not found in active set
0x55e467b563c0 not found in active set
Focusing 0x7ffcbd27df80
Focusing 0x55e467b563c0
Setting focus to 0x7ffcbd27e5d0 (parent: 0x55e467b563c0)
```
Could look like:
```
Setting focus to Kayton (parent: Sile)
Kayton not found in active set
Sile not found in active set
Focusing Kayton
Focusing Sile
Setting focus to Grorll (parent: Sile)
```

Each file contains the simple algorithm implemented in a different programming language.

To test:
- `gcc name_of.c && ./a.out`
- `g++ name_of.cpp && ./a.out`
- `python3 name_of.py`

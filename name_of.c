#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Given a pointer, returns a human-readable name. Useful for debugging, since
// names are easier to remember and recognise than memory addresses. 0 results
// in "Null", and Names of inputs that are close together often rhyme.
static const char* name_of(const void* input) {
#define ARR_SIZE(a) (sizeof(a) / sizeof(a[0]))

    static int cache_i = -1;
    static char cache[100][20];
    cache_i = (cache_i + 1) % ARR_SIZE(cache);

    static const char* const beginnings[] = {
        "N", "Al", "Aver", "B", "Ch", "D", "Ell", "Ev", "F", "G", "Gr", "H",
        "Iv", "J", "K", "L", "M", "Mr ", "Ms ", "Mx ", "P", "Pr", "Qu", "R",
        "S", "T", "Tr", "V", "W", "Z"
    };

    static const char* const middles[] = {
        "u", "a", "ai", "ay", "e","ee", "el", "en", "i", "ing", "o", "oo",
        "ou", "or"
    };

    static const char* const ends[] = {
        "ll", "b", "ck", "cy", "d", "dy", "gh", "gia", "r", "l", "le", "liet",
        "lie", "mer", "nd", "ny", "rd", "sly", "son", "ss", "thy", "te", "tt",
        "xon", "y", "yton", "zz", ""
    };

    unsigned long int id = (unsigned long int)input;

    const char* beginning = beginnings[id % ARR_SIZE(beginnings)];
    id /= ARR_SIZE(beginnings);

    const char* middle = middles[id % ARR_SIZE(middles)];
    id /= ARR_SIZE(middles);

    const char* end = ends[id % ARR_SIZE(ends)];
    id /= ARR_SIZE(ends);

    sprintf(cache[cache_i], "%s%s%s", beginning, middle, end);
    return cache[cache_i];

#undef ARR_SIZE
}

int main() {
    srand(time(NULL));
    for (int i = 0; i < 20; i++) {
        unsigned long long num = ((unsigned long long)rand() << 32) | rand();
        printf("%s\n", name_of((void*)num));
    }
}

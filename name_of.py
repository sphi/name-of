#!/usr/bin/env python3

# Given a pointer, returns a human-readable name. Useful for debugging, since
# names are easier to remember and recognise than memory addresses. 0 results
# in "Null", and Names of inputs that are close together often rhyme.
def name_of(value):
    if not isinstance(value, int):
        value = id(value)

    parts = [[
        "N", "Al", "Aver", "B", "Ch", "D", "Ell", "Ev", "F", "G", "Gr", "H",
        "Iv", "J", "K", "L", "M", "Mr ", "Ms ", "Mx ", "P", "Pr", "Qu", "R",
        "S", "T", "Tr", "V", "W", "Z"
    ], [
        "u", "a", "ai", "ay", "e","ee", "el", "en", "i", "ing", "o", "oo",
        "ou", "or"
    ], [
        "ll", "b", "ck", "cy", "d", "dy", "gh", "gia", "r", "l", "le", "liet",
        "lie", "mer", "nd", "ny", "rd", "sly", "son", "ss", "thy", "te", "tt",
        "xon", "y", "yton", "zz", ""
    ]]

    result = ''
    for part in parts:
        result += part[value % len(part)];
        value //= len(part);

    return result

objects = []
for i in range(20):
    obj = object()
    objects.append(obj)
    print(name_of(obj))
